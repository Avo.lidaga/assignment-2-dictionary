section .data
%include "words.inc"

%define buffer_size 0xFF

section .rodata
  not_found: db "string not found", 0
  limit: db "string exceeds limit", 0


section .bss
  buffer: resb buffer_size


section .text
global _start
%include "lib.inc"
extern find_word



_start:
    mov rsi, 256
    mov rdi, buffer
    push rdi
    call read_word
    pop rdi
    cmp rax, 0
    je .limit
    mov rsi, start_dict
    call find_word
    cmp rax, 0
    je .not_found
    add rax, 8
    push rax
    mov rdi, rax
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string                 ; print value
    call print_newline
    xor rdi, rdi
    call exit
  .limit:
    mov rdi, limit
    call print_error                ; string exceeds limit
    call print_newline
    call exit

  .not_found:
    mov rdi, not_found
    call print_error                ; string not found
    call print_newline
    call exit



