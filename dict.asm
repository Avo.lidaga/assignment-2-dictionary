extern string_equals

global find_word

section .text

find_word:
    	.loop:
            cmp rsi, 0         ; проверка на ноль -> конец dictionary
            jz .end

            push rdi
            push rsi
            add rsi, 8          ; указатель на ключ
            call string_equals  ; через string_equals определяем совпали ли строки
                                ; rax == 1, если строки равны, или rax == 0
            pop rsi
            pop rdi

            test rax, rax       ; совпадают -> завершаем, иначе идем к следующему элементу
            jnz .end
            mov rsi, [rsi]
            jmp .loop           ; все по новой

            .end:
            mov rax, rsi
            ret
