%define start_dict 0
%macro colon 2
     %2:
     dq start_dict
     db %1, 0
     %define start_dict %2

%endmacro
